﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Model;
using Business.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net;

namespace WebApplication.Controllers
{
    [Route("api/test")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private TestService testService = new TestService();
        
        [HttpPost]
        [Authorize(Roles = "Admin, Moderator")]
        [Route("add")]
        public async Task<bool> PostTest(TestModel model)
        {
            return await testService.AddAsync(model);
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpGet]
        [Route("get")]
        public async Task<Object> GetTestById(int testId)
        {
            var result = await testService.GetByIdAsync(testId);
            return result;
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpGet]
        [Route("questions")]
        public async Task<Object> GetQuestions(int testId)
        {
            var result = await testService.GetQuestionsAsync(testId);
            return Ok(result);
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpPost]
        [Route("answers")]
        public async Task<Object> GetAnswers(int[] questionIds)
        {
            var result = await testService.GetAnswersAsync(questionIds);
            return Ok(result);
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [Route("all")]
        [HttpGet]
        public async Task<List<TestModel>> GetAllTests()
        {
            return await testService.GetAllAsync();
        }

        [Authorize(Roles = "Admin, Moderator")]
        [HttpPost]
        [Route("changename")]
        public async Task<bool> ChangeName(int testId, string newName)
        {
            var result = await testService.ChangeName(testId, newName);
            return result;
        }

        [Authorize(Roles = "Admin, Moderator")]
        [HttpPost]
        [Route("changesubject")]
        public async Task<bool> ChangeSubject(int testId, string newSubject)
        {
            return await testService.ChangeSubject(testId, newSubject);
        }

        [Authorize(Roles = "Admin, Moderator")]
        [HttpPost]
        [Route("changedescription")]
        public async Task<bool> ChangeDescription(int testId, string newDescription)
        {
            return await testService.ChangeDescription(testId, newDescription);
        }
    }
}
