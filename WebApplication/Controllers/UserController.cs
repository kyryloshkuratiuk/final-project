﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Business.Service;
using Business.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Internal;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using Data.Entities;
using System.Text.RegularExpressions;

namespace WebApplication.Controllers
{
    [Route("api/user/")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService userService = new UserService();
        private UserManager<Data.Entities.UserEntity> userManager;
        private SignInManager<Data.Entities.UserEntity> signInManager;

        public UserController(UserManager<Data.Entities.UserEntity> userManager, SignInManager<Data.Entities.UserEntity> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [Authorize(Roles = "Admin")]
        [Route("add")]
        [HttpGet]
        public async Task<bool> AddUser(string fullName)
        {
            UserModel userModel = new UserModel
            {
                FullName = fullName
            };
            return await userService.AddAsync(userModel);
        }

        [Authorize(Roles = "Admin, Moderator")]
        [Route("all")]
        [HttpGet]
        public async Task<List<UserModel>> GetAllUsers()
        {
            List<UserModel> userModels = new List<UserModel>();
            return await userService.GetAllAsync();
        }

        [HttpPost]
        [Route("register")]
        public async Task<Object> PostUser(UserModel model)
        {
            try
            {
                model.Role = "Customer";
                var user = userService.mapper.Map<Data.Entities.UserEntity>(model);
                var result = await userManager.CreateAsync(user, model.Password);
                await userManager.AddToRoleAsync(user, model.Role);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(Business.Model.LoginModel model)
        {
            var user = await userManager.FindByNameAsync(model.Username);
            if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
            {
                var role = await userManager.GetRolesAsync(user);
                IdentityOptions options = new IdentityOptions();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID", user.Id.ToString()),
                        new Claim(options.ClaimsIdentity.RoleClaimType, role.FirstOrDefault())
                    }
                    ),
                    Expires = DateTime.UtcNow.AddDays(1), // \/ need to move security key to appsettings. 42:00 min in tutorial
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("KiRillShkuratiuk")), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new { token });

            }
            else
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpPost]
        [Route("changepassword")]
        public async Task<bool> ChangePassword(string newPassword)
        {
            var user = await userManager.FindByIdAsync(User.Claims.First(c => c.Type == "UserID").Value);
            var passwordToken = await userManager.GeneratePasswordResetTokenAsync(user);
            var passwordValidator = new PasswordValidator<Data.Entities.UserEntity>();
            var result = await passwordValidator.ValidateAsync(userManager, null, newPassword);
            if (result.Succeeded)
            {
                await userManager.ResetPasswordAsync(user, passwordToken, newPassword);
                return true;
            }
            else
            {
                return false;
            }
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpPost]
        [Route("changeemail")]
        public async Task<bool> ChangeEmail(string newEmail)
        {
            var user = await userManager.FindByIdAsync(User.Claims.First(c => c.Type == "UserID").Value);
            bool isValid = true;
            if (Regex.IsMatch(newEmail, @"^[^@\s]+@[^@\s]+\.[^@\s]+$"))
            {
                foreach (var userEntity in userManager.Users)
                {
                    if (userEntity.Email == newEmail)
                    {
                        if (userEntity.Id == user.Id)
                        {
                            isValid = true;
                            break;
                        }
                        else
                        {
                            isValid = false;
                        }
                    }
                }
            }
            else
            {
                isValid = false;
            }
            if (isValid == true)
            {
                var token = await userManager.GenerateChangeEmailTokenAsync(user, newEmail);
                await userManager.ChangeEmailAsync(user, newEmail, token);
                return true;
            }
            else 
            {
                return false;
            }
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpPost]
        [Route("changefullname")]
        public async Task<bool> ChangeFullName(string newFullName)
        {
            var user = await userManager.FindByIdAsync(User.Claims.First(c => c.Type == "UserID").Value);
            if (newFullName != null)
            {
                user.FullName = newFullName;
                await userManager.UpdateAsync(user);
                return true;
            }
            else
            {
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("changefullnameadmin")]
        public async Task<bool> ChangeFullNameAdmin(string userId, string newFullName)
        {
            try
            {
                var user = await userManager.FindByIdAsync(userId);
                if (newFullName != null && user != null)
                {
                    user.FullName = newFullName;
                    await userManager.UpdateAsync(user);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("changeemailadmin")]
        public async Task<bool> ChangeEmailAdmin(string userId, string newEmail)
        {
            try
            {
                var user = await userManager.FindByIdAsync(userId);
                bool isValid = true;
                if (Regex.IsMatch(newEmail, @"^[^@\s]+@[^@\s]+\.[^@\s]+$"))
                {
                    foreach (var userEntity in userManager.Users)
                    {
                        if (userEntity.Email == newEmail)
                        {
                            if (userEntity.Id == user.Id)
                            {
                                isValid = true;
                                break;
                            }
                            else
                            {
                                isValid = false;
                            }
                        }
                    }
                }
                else
                {
                    isValid = false;
                }
                if (isValid == true)
                {
                    var token = await userManager.GenerateChangeEmailTokenAsync(user, newEmail);
                    await userManager.ChangeEmailAsync(user, newEmail, token);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("changepasswordadmin")]
        public async Task<bool> ChangePasswordAdmin(string userId, string newPassword)
        {
            try
            {
                var user = await userManager.FindByIdAsync(userId);
                var passwordToken = await userManager.GeneratePasswordResetTokenAsync(user);
                var passwordValidator = new PasswordValidator<Data.Entities.UserEntity>();
                var result = await passwordValidator.ValidateAsync(userManager, null, newPassword);
                if (result.Succeeded)
                {
                    await userManager.ResetPasswordAsync(user, passwordToken, newPassword);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("update")]
        public void Update(UserModel model)
        {
            userService.Update(model);
        }
    }
}
