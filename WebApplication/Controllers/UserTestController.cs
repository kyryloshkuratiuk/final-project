﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Model;
using Business.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers
{
    [Route("api/stats")]
    [ApiController]
    public class UserTestController : ControllerBase
    {
        UserTestService userTestService = new UserTestService();

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = "Customer, Admin, Moderator")]
        public async Task<bool> AddAsync(UserTestModel model)
        {
            return await userTestService.AddAsync(model);
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpGet]
        [Route("getstats")]
        public async Task<Object> GetUserProfile()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            return await userTestService.GetStats(userId);
        }

        [Authorize(Roles = "Admin, Moderator")]
        [HttpGet]
        [Route("all")]
        public async Task<Object> GetAllStats()
        {
            return await userTestService.GetAllAsync();
        }
    }
}
