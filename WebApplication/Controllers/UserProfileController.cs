﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers
{
    [Route("api/profile")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private UserManager<Data.Entities.UserEntity> userManager;

        public UserProfileController(UserManager<Data.Entities.UserEntity> userManager)
        {
            this.userManager = userManager;
        }

        [Authorize(Roles = "Customer, Admin, Moderator")]
        [HttpGet]
        public async Task<Object> GetUserProfile()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            var user = await userManager.FindByIdAsync(userId);
            if (user != null)
            {
                return new
                {
                    user.Id,
                    user.FullName,
                    user.Email,
                    user.UserName
                };
            }
            else return null;
        }
    }
}
