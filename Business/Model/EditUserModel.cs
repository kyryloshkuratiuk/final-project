﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Model
{
    public class EditUserModel
    {
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
    }
}
