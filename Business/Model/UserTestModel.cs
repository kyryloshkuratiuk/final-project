﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Model
{
    public class UserTestModel
    {
        public string UserEntityId { get; set; }
        public int TestEntityId { get; set; }
        public int Score { get; set; }
        public int TimeSpent { get; set; }
        public string Date { get; set; }
    }
}
