﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Model
{
    public class TestModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Subject { get; set; }

        public string Description { get; set; }

        public double UsersRate { get; set; }

        public int Participants { get; set; }

        public double AverageTimeInSeconds { get; set; }

        public ICollection<QuestionEntity> Questions { get; set; }
    }
}
