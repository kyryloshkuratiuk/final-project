﻿using AutoMapper;
using Business.Model;
using Data.Entities;

namespace Business.AutoMapper
{
    class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<TestModel, TestEntity>()
                .ForMember(m => m.Id, e => e.MapFrom(src => src.Id))
                .ForMember(m => m.Name, e => e.MapFrom(src => src.Name))
                .ForMember(m => m.Subject, e => e.MapFrom(src => src.Subject))
                .ForMember(m => m.Description, e => e.MapFrom(src => src.Description))
                .ForMember(m => m.UsersRate, e => e.MapFrom(src => src.UsersRate))
                .ForMember(m => m.Participants, e => e.MapFrom(src => src.Participants))
                .ForMember(m => m.AverageTimeInSeconds, e => e.MapFrom(src => src.AverageTimeInSeconds));
                //.ForMember(m => m.Option1, e => e.MapFrom(src => src.Option1))
                //.ForMember(m => m.Option2, e => e.MapFrom(src => src.Option2))
                //.ForMember(m => m.Option3, e => e.MapFrom(src => src.Option3))
                //.ForMember(m => m.Option4, e => e.MapFrom(src => src.Option4))
                //.ForMember(m => m.CorrectOption, e => e.MapFrom(src => src.CorrectOption))
                //.ForMember(m => m.Explanation, e => e.MapFrom(src => src.Explanation))
                //.ForMember(m => m.TimeForOneQuestionInSeconds, e => e.MapFrom(src => src.TimeForOneQuestionInSeconds));

            CreateMap<TestEntity, TestModel>()
                .ForMember(e => e.Id, m => m.MapFrom(src => src.Id))
                .ForMember(e => e.Name, m => m.MapFrom(src => src.Name))
                .ForMember(e => e.Subject, m => m.MapFrom(src => src.Subject))
                .ForMember(e => e.Description, m => m.MapFrom(src => src.Description))
                .ForMember(e => e.UsersRate, m => m.MapFrom(src => src.UsersRate))
                .ForMember(e => e.Participants, m => m.MapFrom(src => src.Participants))
                .ForMember(e => e.AverageTimeInSeconds, m => m.MapFrom(src => src.AverageTimeInSeconds));
                //.ForMember(e => e.Option1, m => m.MapFrom(src => src.Option1))
                //.ForMember(e => e.Option2, m => m.MapFrom(src => src.Option2))
                //.ForMember(e => e.Option3, m => m.MapFrom(src => src.Option3))
                //.ForMember(e => e.Option4, m => m.MapFrom(src => src.Option4))
                //.ForMember(e => e.CorrectOption, m => m.MapFrom(src => src.CorrectOption))
                //.ForMember(e => e.Explanation, m => m.MapFrom(src => src.Explanation))
                //.ForMember(e => e.TimeForOneQuestionInSeconds, m => m.MapFrom(src => src.TimeForOneQuestionInSeconds));

            CreateMap<UserModel, UserEntity>()
                .ForMember(m => m.FullName, e => e.MapFrom(src => src.FullName))
                .ForMember(m => m.Email, e => e.MapFrom(src => src.Email))
                .ForMember(m => m.UserName, e => e.MapFrom(src => src.Username));

            CreateMap<UserEntity, UserModel>()
                .ForMember(m => m.FullName, e => e.MapFrom(src => src.FullName));

            CreateMap<UserTestEntity, UserTestModel>();

            CreateMap<UserTestModel, UserTestEntity>();
        }
    }
}
