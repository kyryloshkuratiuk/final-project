﻿using Business.Interfaces;
using Business.Model;
using System;
using System.Collections.Generic;
using Data.Interfaces;
using Data.Entities;
using AutoMapper;
using System.Threading.Tasks;
using Business.AutoMapper;
using System.Linq;

namespace Business.Service
{
    public class TestService : ITestService
    {
        public IUnitOfWork unitOfWork = new Data.UnitOfWork.UnitOfWork();
        private Mapper mapper = new Mapper(new MapperConfiguration(cfg => { cfg.AddProfile<AutoMapperProfile>(); }));


        /// <summary>
        /// Adding test entity to the database
        /// </summary>
        /// <param name="model">TestModel object</param>
        /// <returns>Boolean value</returns>
        public async Task<bool> AddAsync(TestModel model)
        {
            try
            {
                TestEntity test = mapper.Map<TestEntity>(model);
                var result = await unitOfWork.TestRepository.AddAsync(test);
                if (result.Id > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete test entity by id
        /// </summary>
        /// <param name="id">Test id</param>
        /// <returns>Boolean value</returns>
        public bool DeleteById(int id)
        {
            if (unitOfWork.TestRepository.GetEntityByIdAsync(id) != null)
            {
                unitOfWork.TestRepository.Delete(id);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get all test entities from the database
        /// </summary>
        /// <returns>List of TestModel objects</returns>
        public async Task<List<TestModel>> GetAllAsync()
        {
            try
            {
                List<TestEntity> testEntities = await unitOfWork.TestRepository.GetEntitiesAsync();
                List<TestModel> testModels = new List<TestModel>();
                foreach (TestEntity testEntity in testEntities)
                {
                    testModels.Add(mapper.Map<TestModel>(testEntity));
                }
                return testModels;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get questions of test
        /// </summary>
        /// <param name="testId">Id of test</param>
        /// <returns>IEnumerable of question objects</returns>
        public async Task<IEnumerable<object>> GetQuestionsAsync(int testId)
        {
            var list = await unitOfWork.QuestionRepository.GetEntitiesAsync();
            var questions = list
                .Where(x => x.TestEntityId == testId)
                .Select(x => new
                 {
                     Id = x.Id,
                     Question = x.Question,
                     Option1 = x.Option1,
                     Option2 = x.Option2,
                     Option3 = x.Option3,
                     Option4 = x.Option4,
                     Option5 = x.Option5,
                     CorrectOption = x.CorrectOption,
                     Explanation = x.Explanation,
                     Timer = x.Timer
                 })
                .ToArray();
            var updated = questions.AsEnumerable()
                .Select(x => new
                {
                    Id = x.Id,
                    TestId = testId,
                    Question = x.Question,
                    Options = new string[] { x.Option1, x.Option2, x.Option3, x.Option4, x.Option5 },
                    Timer = x.Timer
                });
            return updated;
        }

        /// <summary>
        /// Get answers to guestions
        /// </summary>
        /// <param name="questionIds">Array of question ids</param>
        /// <returns>IEnumerable of answer objects</returns>
        public async Task<IEnumerable<object>> GetAnswersAsync(int[] questionIds)
        {
            var list = await unitOfWork.QuestionRepository.GetEntitiesAsync();
            var answers = list
                .AsEnumerable()
                .Where(y => questionIds.Contains(y.Id))
                .OrderBy(x => { return Array.IndexOf(questionIds, x.Id); })
                .Select(z => z.CorrectOption)
                .ToArray();
            return answers;
        }


        /// <summary>
        /// Get TestModel by id
        /// </summary>
        /// <param name="id">Test model id</param>
        /// <returns>TestModel object</returns>
        public async Task<TestModel> GetByIdAsync(int id)
        {
            return mapper.Map<TestModel>(await unitOfWork.TestRepository.GetEntityByIdAsync(id));
        }

        /// <summary>
        /// Change name of test
        /// </summary>
        /// <param name="testId">Test Id</param>
        /// <param name="newName">New name</param>
        /// <returns>Boolean value</returns>
        public async Task<bool> ChangeName(int testId, string newName)
        {
            try
            {
                var test = await unitOfWork.TestRepository.GetEntityByIdAsync(testId);
                if (newName != null && test != null)
                {
                    test.Name = newName;
                    unitOfWork.TestRepository.Update(mapper.Map<TestEntity>(test));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Changes subject of test
        /// </summary>
        /// <param name="testId">Id of test</param>
        /// <param name="newSubject">Subject of test</param>
        /// <returns>Boolean value</returns>
        public async Task<bool> ChangeSubject(int testId, string newSubject)
        {
            try 
            {
                var test = await unitOfWork.TestRepository.GetEntityByIdAsync(testId);
                if (newSubject != null && test != null)
                {
                    test.Subject = newSubject;
                    unitOfWork.TestRepository.Update(mapper.Map<TestEntity>(test));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Changes description of test
        /// </summary>
        /// <param name="testId">Id of test</param>
        /// <param name="newDescription">Description of test</param>
        /// <returns>Boolean value</returns>
        public async Task<bool> ChangeDescription(int testId, string newDescription)
        {
            try
            {
                var test = await unitOfWork.TestRepository.GetEntityByIdAsync(testId);
                if (newDescription != null && test != null)
                {
                    test.Description = newDescription;
                    unitOfWork.TestRepository.Update(mapper.Map<TestEntity>(test));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Test Model
        /// </summary>
        /// <param name="model">TestModel object</param>
        public void Update(TestModel model)
        {
            unitOfWork.TestRepository.Update(mapper.Map<TestEntity>(model));
        }
    }
}
