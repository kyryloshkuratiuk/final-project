﻿using AutoMapper;
using Business.AutoMapper;
using Business.Interfaces;
using Business.Model;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Business.Service
{
    public class UserTestService : IUserTestService
    {
        public IUnitOfWork unitOfWork = new Data.UnitOfWork.UnitOfWork();
        private Mapper mapper = new Mapper(new MapperConfiguration(cfg => { cfg.AddProfile<AutoMapperProfile>(); }));

        /// <summary>
        /// Add UserTestEntity to the database
        /// </summary>
        /// <param name="model">UserTestModel object</param>
        /// <returns>Boolean value</returns>
        public async Task<bool> AddAsync(UserTestModel model)
        {
            try
            {
                UserTestEntity test = mapper.Map<UserTestEntity>(model);
                var result = await unitOfWork.UserTestRepository.AddAsync(test);
                if (result.Id > 0 && result.TestEntityId > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete UserTestEntity from the database
        /// </summary>
        /// <param name="id">Id of UserTestEntity</param>
        /// <returns>Boolean object</returns>
        public bool DeleteById(int id)
        {
            if (unitOfWork.UserTestRepository.GetEntityByIdAsync(id) != null)
            {
                unitOfWork.UserTestRepository.Delete(id);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get all UserTestEntities from the database
        /// </summary>
        /// <returns>List of UserTestModels</returns>
        public async Task<List<UserTestModel>> GetAllAsync()
        {
            try
            {
                List<UserTestEntity> userTestEntities = await unitOfWork.UserTestRepository.GetEntitiesAsync();
                List<UserTestModel> userTestModels = new List<UserTestModel>();
                foreach (UserTestEntity userTestEntity in userTestEntities)
                {
                    userTestModels.Add(mapper.Map<UserTestModel>(userTestEntity));
                }
                return userTestModels;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get UserTestModel by id
        /// </summary>
        /// <param name="id">UserTestEntity id</param>
        /// <returns>UserTestModel object</returns>
        public async Task<UserTestModel> GetByIdAsync(int id)
        {
            return mapper.Map<UserTestModel>(unitOfWork.UserTestRepository.GetEntityByIdAsync(id));
        }

        /// <summary>
        /// Update UserTestModel object
        /// </summary>
        /// <param name="model">UserTestModel object</param>
        public void Update(UserTestModel model)
        {
            unitOfWork.UserTestRepository.Update(mapper.Map<UserTestEntity>(model));
        }

        /// <summary>
        /// Get statistics of user by id
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Statistics object</returns>
        public async Task<Object> GetStats(string userId)
        {
            var userTestEntities = await unitOfWork.UserTestRepository.GetEntitiesAsync();
            var stats = userTestEntities
                .Where(user => user.UserEntityId == userId);
            return stats;
        }
    }
}
