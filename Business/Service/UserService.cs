﻿using AutoMapper;
using Business.AutoMapper;
using Business.Interfaces;
using Business.Model;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Service
{
    public class UserService : IUserService
    {
        public static UserService instance;

        public IUnitOfWork unitOfWork = new Data.UnitOfWork.UnitOfWork();

        public Mapper mapper = new Mapper(new MapperConfiguration(cfg => { cfg.AddProfile<AutoMapperProfile>(); }));

        /// <summary>
        /// Add user entity to the database
        /// </summary>
        /// <param name="model">UserModel object</param>
        /// <returns>Boolean value</returns>
        public async Task<bool> AddAsync(UserModel model)
        {
            try
            {
                //var user = new UserEntity
                //{
                //    Id = model.Id,
                //    Email = model.Email,
                //    Password = model.Password,
                //    RoleId = model.RoleId,
                //    Role = model.RoleName
                //};
                var user = mapper.Map<UserEntity>(model);
                var result = await unitOfWork.UserRepository.AddAsync(user);
                if (int.Parse(result.Id) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception error)
            {
                return false;
            }
            //check if user is valid if(user.x == "") throw
            //var user = mapper.Map<UserEntity>(model);
        }

        /// <summary>
        /// Delete user entity from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteById(int id)
        {
            if (unitOfWork.UserRepository.GetEntityByIdAsync(id) != null)
            {
                unitOfWork.UserRepository.Delete(id);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get all users from the database
        /// </summary>
        /// <returns>List of User Models</returns>
        public async Task<List<UserModel>> GetAllAsync()
        {
            return mapper.Map<List<UserModel>>(await unitOfWork.UserRepository.GetEntitiesAsync());
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>UserModel object</returns>
        public async Task<UserModel> GetByIdAsync(int id)
        {
            //return mapper.Map<UserModel>(unitOfWork.UserRepository.GetEntity(id));
            return null;
        }

        /// <summary>
        /// Update user model
        /// </summary>
        /// <param name="model">UserModel object</param>
        public void Update(UserModel model)
        {
            UserEntity userEntity = mapper.Map<UserEntity>(model);
            unitOfWork.UserRepository.Update(userEntity);
        }
    }
}
