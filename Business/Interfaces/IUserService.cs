﻿using Business.Model;
using Data.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    interface IUserService : ICrud<UserModel>
    {
    }
}
