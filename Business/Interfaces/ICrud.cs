﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    interface ICrud<TModel> where TModel : class
    {
        Task<List<TModel>> GetAllAsync();

        Task<TModel> GetByIdAsync(int id);

        Task<bool> AddAsync(TModel model);

        void Update(TModel model);

        bool DeleteById(int id);
    }
}
