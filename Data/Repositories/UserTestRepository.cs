﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UserTestRepository : IUserTestRepository
    {
        public async Task<UserTestEntity> AddAsync(UserTestEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                await context.UserTestEntities.AddAsync(entity);
                await context.SaveChangesAsync();
            }
            return entity;
        }

        public void Delete(int id)
        {
            UserTestEntity foundItem;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                foundItem = context.UserTestEntities.FirstOrDefault(i => i.Id == id);
                context.Remove(foundItem);
                context.SaveChangesAsync();
            }
        }

        public async Task<List<UserTestEntity>> GetEntitiesAsync()
        {
            List<UserTestEntity> userTestEntities;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                userTestEntities = await context.UserTestEntities.ToListAsync();
            }
            return userTestEntities;
        }

        public List<UserTestEntity> GetEntities()
        {
            List<UserTestEntity> userTestEntities;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                userTestEntities = context.UserTestEntities.ToList();
            }
            return userTestEntities;
        }

        public async Task<UserTestEntity> GetEntityByIdAsync(int id)
        {
            UserTestEntity userTestEntity;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                userTestEntity = await context.UserTestEntities.FirstOrDefaultAsync(i => i.Id == id);
            }
            return userTestEntity;
        }

        public async Task<int> SaveAsync()
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                return await context.SaveChangesAsync();
            }
        }

        public async void Update(UserTestEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                context.Entry(entity).State = EntityState.Modified;
            }
        }
    }
}
