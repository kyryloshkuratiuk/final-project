﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        public async Task<UserEntity> AddAsync(UserEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                await context.UserEntities.AddAsync(entity);
                await context.SaveChangesAsync();
            }
            return entity;
        }

        public async Task<UserEntity> AddAsync(UserEntity entity, string password)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                await context.UserEntities.AddAsync(entity);
                await context.SaveChangesAsync();
            }
            return entity;
        }

        public void Delete(int id)
        {
            UserEntity foundItem;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                foundItem = context.UserEntities.FirstOrDefault(i => int.Parse(i.Id) == id);
                context.Remove(foundItem);
                context.SaveChangesAsync();
            }
        }

        public async Task<List<UserEntity>> GetEntitiesAsync()
        {
            List<UserEntity> userEntities;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                userEntities = await context.UserEntities.ToListAsync();
            }
            return userEntities;
        }

        public async Task<UserEntity> GetEntityByIdAsync(int id)
        {
            UserEntity userEntity;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                userEntity = await context.UserEntities.FirstOrDefaultAsync(i => int.Parse(i.Id) == id);
            }
            return userEntity;
        }

        public async Task<int> SaveAsync()
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                return await context.SaveChangesAsync();
            }
        }

        public void Update(UserEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChangesAsync();
            }
        }

        public async Task<Object> Register(UserEntity userEntity, string password, UserManager<UserEntity> userManager)
        {
            try
            {
                using (var context = userManager)
                {
                    //return await userManager.CreateAsync(userEntity, password);
                    userEntity.PasswordHash = password;
                    return await context.CreateAsync(userEntity, password);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
