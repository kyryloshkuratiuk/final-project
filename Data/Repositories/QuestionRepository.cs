﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    class QuestionRepository : IQuestionRepository
    {
        public async Task<QuestionEntity> AddAsync(QuestionEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                await context.QuestionEntities.AddAsync(entity);
                await context.SaveChangesAsync();
            }
            return entity;
        }

        public void Delete(int id)
        {
            QuestionEntity foundItem;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                foundItem = context.QuestionEntities.FirstOrDefault(i => i.Id == id);
                context.Remove(foundItem);
                context.SaveChangesAsync();
            }
        }

        public async Task<List<QuestionEntity>> GetEntitiesAsync()
        {
            List<QuestionEntity> questionEntities;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                questionEntities = await context.QuestionEntities.ToListAsync();
            }
            return questionEntities;
        }

        public async Task<QuestionEntity> GetEntityByIdAsync(int id)
        {
            QuestionEntity questionEntity;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                questionEntity = await context.QuestionEntities.FirstOrDefaultAsync(i => i.Id == id);
            }
            return questionEntity;
        }

        public async Task<int> SaveAsync()
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                return await context.SaveChangesAsync();
            }
        }

        public void Update(QuestionEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                context.Entry(entity).State = EntityState.Modified;
            }
        }
    }
}
