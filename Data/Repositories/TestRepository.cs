﻿using Data.Context;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class TestRepository : ITestRepository
    {
        public async Task<TestEntity> AddAsync(TestEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                await context.TestEntities.AddAsync(entity);
                await context.SaveChangesAsync();
            }
            return entity;
        }

        public void Delete(int id)
        {
            TestEntity foundItem;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                foundItem = context.TestEntities.FirstOrDefault(i => i.Id == id);
                context.Remove(foundItem);
                context.SaveChangesAsync();
            }
        }

        public async Task<List<TestEntity>> GetEntitiesAsync()
        {
            List<TestEntity> testEntities;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                testEntities = await context.TestEntities.ToListAsync();
            }
            return testEntities;
        }

        public async Task<TestEntity> GetEntityByIdAsync(int id)
        {
            TestEntity testEntity;
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                testEntity = await context.TestEntities.FirstOrDefaultAsync(i => i.Id == id);
            }
            return testEntity;
        }

        public async Task<int> SaveAsync()
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                return await context.SaveChangesAsync();
            }
        }

        public void Update(TestEntity entity)
        {
            using (var context = new DatabaseContext(DatabaseContext.options.databaseOptions))
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
