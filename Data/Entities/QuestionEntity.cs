﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class QuestionEntity : BaseEntity
    {
        [ForeignKey("TestEntity")]
        public int TestEntityId { get; set; }

        public string Question { get; set; }

        public string Option1 { get; set; }

        public string Option2 { get; set; }

        public string Option3 { get; set; }

        public string Option4 { get; set; }

        public string Option5 { get; set; }

        public string CorrectOption { get; set; }

        public string Explanation { get; set; }

        public double Timer { get; set; }
    }
}
