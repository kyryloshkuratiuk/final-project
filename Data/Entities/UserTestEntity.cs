﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entities
{
    public class UserTestEntity : BaseEntity
    {
        public string UserEntityId { get; set; }
        public int TestEntityId { get; set; }
        public int Score { get; set; }
        public int TimeSpent { get; set; }
        public string Date { get; set; }
    }
}
