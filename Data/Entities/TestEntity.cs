﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class TestEntity : BaseEntity
    {
        public string Name { get; set; }

        public string Subject { get; set; }

        public string Description { get; set; }

        [ForeignKey("TestEntityId")]
        public virtual ICollection<QuestionEntity> Questions { get; set; }

        public double UsersRate { get; set; }

        public int Participants { get; set; }

        public double AverageTimeInSeconds { get; set; }
    }
}
