﻿using Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Data.Context
{
    public class DatabaseContext : IdentityDbContext
    {
        public class OptionsBuild
        {
            public OptionsBuild()
            {
                settings = new AppConfiguration();
                optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
                optionsBuilder.UseSqlServer(settings.SqlConnectionString);
                databaseOptions = optionsBuilder.Options;
            }
            public DbContextOptionsBuilder<DatabaseContext> optionsBuilder { get; set; }
            public DbContextOptions<DatabaseContext> databaseOptions { get; set; }
            private AppConfiguration settings { get; set; }
        }

        public static OptionsBuild options = new OptionsBuild();

        public static DatabaseContext instance;
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        public DbSet<UserEntity> UserEntities { get; set; }
        public DbSet<TestEntity> TestEntities { get; set; }
        public DbSet<QuestionEntity> QuestionEntities { get; set; }
        public DbSet<UserTestEntity> UserTestEntities { get; set; }
    }
}
