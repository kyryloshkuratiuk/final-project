﻿using Data.Context;
using Data.Interfaces;
using Data.Repositories;

namespace Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private UserRepository userRepository;
        private TestRepository testRepository;
        private QuestionRepository questionRepository;
        private UserTestRepository userTestRepository;
        
        public IUserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository();
                }
                return userRepository;
            }
        }

        public ITestRepository TestRepository
        {
            get
            {
                if (testRepository == null)
                {
                    testRepository = new TestRepository();
                }
                return testRepository;
            }
        }

        public IQuestionRepository QuestionRepository
        {
            get
            {
                if (questionRepository == null)
                {
                    questionRepository = new QuestionRepository();
                }
                return questionRepository;
            }
        }

        public IUserTestRepository UserTestRepository
        {
            get
            {
                if (userTestRepository == null)
                {
                    userTestRepository = new UserTestRepository();
                }
                return userTestRepository;
            }
        }
    }
}
