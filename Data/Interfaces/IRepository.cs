﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IRepository<TEntity> //where TEntity : BaseEntity
    {
        Task<List<TEntity>> GetEntitiesAsync();
        Task<TEntity> GetEntityByIdAsync(int id);
        Task<TEntity> AddAsync(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);
        Task<int> SaveAsync();
        // add soft delete
    }
}
