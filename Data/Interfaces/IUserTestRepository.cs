﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Interfaces
{
    public interface IUserTestRepository : IRepository<UserTestEntity>
    {
        List<UserTestEntity> GetEntities();
    }
}
