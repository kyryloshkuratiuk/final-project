﻿namespace Data.Interfaces
{
    public interface IUnitOfWork
    {
        ITestRepository TestRepository { get; }
        IUserRepository UserRepository { get; }
        IQuestionRepository QuestionRepository { get; }
        IUserTestRepository UserTestRepository { get; }
        //void Dispose();
    }
}
